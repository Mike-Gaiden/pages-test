const filePath = 'https://gitlab.com/your-username/your-repo/-/raw/master/path/to/your/file.txt';

const fileTable = document.getElementById('fileTable');
const tbody = fileTable.querySelector('tbody');

fetch(filePath)
  .then(response => response.text())
  .then(content => {
    const lines = content.split('\n');
    lines.forEach((line, index) => {
      const row = document.createElement('tr');

      const lineNumberCell = document.createElement('td');
      lineNumberCell.textContent = index + 1;
      row.appendChild(lineNumberCell);

      const contentCell = document.createElement('td');
      contentCell.textContent = line;
      row.appendChild(contentCell);

      tbody.appendChild(row);
    });
  })
  .catch(error => {
    console.error('Une erreur s\'est produite :', error);
  });
